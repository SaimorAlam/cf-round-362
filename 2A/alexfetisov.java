import java.io.OutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.StringTokenizer;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;

/**
 * Built using CHelper plug-in
 * Actual solution is at the top
 *
 * @author AlexFetisov
 */
public class Main {
    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        TaskA2_362 solver = new TaskA2_362();
        solver.solve(1, in, out);
        out.close();
    }

    static class TaskA2_362 {
        public void solve(int testNumber, InputReader in, PrintWriter out) {
            int t = in.nextInt();
            int s = in.nextInt();
            int x = in.nextInt();
            if (t > x) {
            out.println("NO");
            return;
        }

            if (t == x) {
                out.println("YES");
                return;
            }
            if (t + 1 == x && s != 1) {
                out.println("NO");
                return;
            }
            if (t + 1 == x && s == 1) {
                out.println("YES");
                return;
            }

            int val = x - t;
            if (val % s == 0) {
                out.println("YES");
                return;
            }
            val = x - t - 1;
            if (val >= 0 && val % s == 0) {
                out.println("YES");
                return;
            }
            out.println("NO");
        }

    }

    static class InputReader {
        private BufferedReader reader;
        private StringTokenizer stt;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream));
        }

        public String nextLine() {
            try {
                return reader.readLine();
            } catch (IOException e) {
                return null;
            }
        }

        public String nextString() {
            while (stt == null || !stt.hasMoreTokens()) {
                stt = new StringTokenizer(nextLine());
            }
            return stt.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(nextString());
        }

    }
}

