#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
using namespace __gnu_pbds;
using namespace std;
#define Foreach(i, c) for(__typeof((c).begin()) i = (c).begin(); i != (c).end(); ++i)
#define For(i,a,b) for(int (i)=(a);(i) < (b); ++(i))
#define rof(i,a,b) for(int (i)=(a);(i) > (b); --(i))
#define rep(i, c) for(auto &(i) : (c))
#define x first
#define y second
#define pb push_back
#define PB pop_back()
#define iOS ios_base::sync_with_stdio(false)
#define sqr(a) (((a) * (a)))
#define all(a) a.begin() , a.end()
#define error(x) cerr << #x << " = " << (x) <<endl
#define Error(a,b) cerr<<"( "<<#a<<" , "<<#b<<" ) = ( "<<(a)<<" , "<<(b)<<" )\n";
#define errop(a) cerr<<#a<<" = ( "<<((a).x)<<" , "<<((a).y)<<" )\n";
#define coud(a,b) cout<<fixed << setprecision((b)) << (a)
#define L(x) ((x)<<1)
#define R(x) (((x)<<1)+1)
#define umap unordered_map
#define double long double
typedef long long ll;
typedef pair<int,int>pii;
typedef vector<int> vi;
typedef complex<double> point;
template <typename T> using os =  tree<T, null_type, less<T>, rb_tree_tag, tree_order_statistics_node_update>;
template <class T>  inline void smax(T &x,T y){ x = max((x), (y));}
template <class T>  inline void smin(T &x,T y){ x = min((x), (y));}
const int maxn = 200 + 10;
struct matrix{
	ll a[maxn][maxn];
	int n, m;
	matrix(){
		n = m = 0;
		memset(a, -1, sizeof a);
	}
	matrix(int n, int m): n(n), m(m){
		memset(a, -1, sizeof a);
	}
	inline void I(){
		For(i,0,n)	a[i][i] = 0;
	}
	inline matrix operator * (const matrix &x){
		matrix ans(n, x.m);
		For(k,0,m)
			For(i,0,n)
				For(j,0,x.m)	if(~a[i][k] && ~x.a[k][j])
					smax(ans.a[i][j], a[i][k] + x.a[k][j]);
		return ans;
	}
};
string s[maxn];
int tr[maxn][27], au[maxn][27], nx = 1, e[maxn], a[maxn], f[maxn], n;
ll l;
inline void add(int i){
	int v = 0;
	rep(ch, s[i]){
		int c = ch - 'a';
		if(!tr[v][c])
			tr[v][c] = nx ++;
		v = tr[v][c];
	}
	e[v] += a[i];
}
inline void ahoc(){
	queue<int> q;
	q.push(0);
	while(!q.empty()){
		int v = q.front();
		q.pop();
		if(f[v] != v)	e[v] += e[f[v]];
		For(i,0,26){
			int u = tr[v][i];
			if(u){
				au[v][i] = u;
				if(v)
					f[u] = au[f[v]][i];
				q.push(u);
			}
			else
				au[v][i] = au[f[v]][i];
		}
	}
}
inline matrix power(matrix a, ll b){
	matrix c(a.n, a.m);
	c.I();
	while(b){
		if(b & 1)
			c = c * a;
		a = a * a;
		b >>= 1;
	}
	return c;
}
int main(){
	iOS;
	cin >> n >> l;
	For(i,0,n)	cin >> a[i];
	For(i,0,n){
		cin >> s[i];
		add(i);
	}
	ahoc();
	matrix dp(nx, nx);
	For(v,0,nx)
		For(i,0,26)
			smax(dp.a[au[v][i]][v], (ll)e[au[v][i]]);
	dp = power(dp, l);
	matrix M(nx, 1);
	M.a[0][0] = 0;
	M = dp * M;
	ll ans = 0;
	For(i,0,nx)
		smax(ans, M.a[i][0]);
	cout << ans << endl;
	return 0;
}
