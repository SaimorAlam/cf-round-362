#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
using namespace __gnu_pbds;
using namespace std;
#define Foreach(i, c) for(__typeof((c).begin()) i = (c).begin(); i != (c).end(); ++i)
#define For(i,a,b) for(int (i)=(a);(i) < (b); ++(i))
#define rof(i,a,b) for(int (i)=(a);(i) > (b); --(i))
#define rep(i, c) for(auto &(i) : (c))
#define x first
#define y second
#define X	first
#define Y	second
#define pb push_back
#define PB pop_back()
#define iOS ios_base::sync_with_stdio(false)
#define sqr(a) (((a) * (a)))
#define all(a) a.begin() , a.end()
#define error(x) cerr << #x << " = " << (x) <<endl
#define Error(a,b) cerr<<"( "<<#a<<" , "<<#b<<" ) = ( "<<(a)<<" , "<<(b)<<" )\n";
#define errop(a) cerr<<#a<<" = ( "<<((a).x)<<" , "<<((a).y)<<" )\n";
#define coud(a,b) cout<<fixed << setprecision((b)) << (a)
#define L(x) ((x)<<1)
#define R(x) (((x)<<1)+1)
#define umap unordered_map
#define double long double
typedef long long ll;
typedef pair<int,int>pii;
typedef vector<int> vi;
typedef pair<double, double> point;
template <typename T> using os =  tree<T, null_type, less<T>, rb_tree_tag, tree_order_statistics_node_update>;
template <class T>  inline void smax(T &x,T y){ x = max((x), (y));}
template <class T>  inline void smin(T &x,T y){ x = min((x), (y));}
inline point operator - (const point &p, const point &q){return point(p.x - q.x, p.y - q.y);}
inline point operator + (const point &p, const point &q){return point(p.x + q.x, p.y + q.y);}
inline point operator * (const point &p, const point &q){return point(p.x * q.x - p.y * q.y, p.x * q.y + p.y * q.x);}
inline point operator * (const point &p, const double &d){return point(p.x*d, p.y*d);}
inline point operator * (const double &d, const point &p){return point(p.x*d, p.y*d);}
inline point operator / (const point &p, const double &d){return point(p.x/d, p.y/d);}
inline double abs(const point &p){
	return sqrt(p.X * p.X + p.Y * p.Y);
}
const int maxn = 300 + 100;
int len[maxn];
const double eps = 1e-12, inf = 1e10;
pii pol[maxn];
inline short sign(double a) { 
	return abs(a) < eps ? 0 : (a < 0? -1:1) ; 
}
inline point unit(point p){
	return p/ abs(p);
}
inline double cross(point a, point b){
	return a.X * b.Y - a.Y * b.X; 
}
inline double dot(point a, point b){
	return a.x * b.x + a.y * b.y;
}
inline double arg(const point &p){
	complex<double> c(p.x, p.y);
	return arg(c);
}
inline point inter(point a, point b, point c, point d){
	return a + (b-a) * (cross(c-a, d-c)/cross(b-a, d-c));
}
inline point shift(point p, double x){
	return x * unit(p) * point(0, 1);
}
inline double getAlpha( point a , point b , point c , point d ) { 
	point cd = d - c;
	return cross( c-a , cd ) / cross( b - a , cd ); 
}
inline bool sect(point a, point b, point c, point d){
	double alpha = getAlpha(a, b, c, d);
	return -eps <= alpha && alpha <= 1 + eps;
}
inline point proj(const point &a, const point &b){	// projection of b on o-a
	return a * dot(a, b) / dot(a, a);
}
inline double dis(const point &a, const point &b, const point &c){
	point h = b + proj(a-b, c-b);
	return abs(h - c);
}

int n;
point p[maxn * 2], q[maxn * 2];
int perm[maxn * 2];
point que[maxn * 2];
int st, en;
inline point mult(const point &p, double d){
	return p * d;
	return point(sign(p.X) * d * abs(p.X), sign(p.Y) * d * abs(p.Y));
}
inline bool add(int i){
	auto x = p[i], y = q[i];
	point c = x + mult(unit( y - x ), -inf); 
	point d = x + mult(unit( y - x ), inf);
	if(st == en){
		que[en ++] = c;
		que[en ++] = d;
		return true;
	}
	while(en > 1 && sign(cross(unit(y-x), que[en-2]-c)) <= 0){
		-- en;
	}
	if(en == 1)	return false;
	if( sign(cross( unit(que[en-1] - que[en-2]) , unit(d-c) )) == 0 ) return true;
	que[en-1] = inter(que[en-2], que[en-1], x, y);
	que[en ++] = d;
	return true;
}
inline bool relax(){
	For(i,0,n)	p[i] = point(pol[i]);
	while(en - st > 3 && sign(cross(que[en-1] - que[en-2], que[st+1] - que[en-2])) <= 0)
		++ st;
	while(en - st > 3 && sign(cross(que[st+1] - que[st]  , que[en-2] - que[st])) <= 0)
		-- en;
	if(!sign(cross( que[st+1] - que[st] , que[en-1] - que[en-2] ))) return 1;
		que[st] = inter(que[st], que[st+1], que[en-2], que[en-1]);
	-- en;
}
inline bool canDo(int l, int r){
	en = st = 0;
	For(j,0,2*n){
		int i = perm[j];
		if(i < n or (l <= i-n && i-n <= r) or (l > r && (i-n <= r or l <= i-n))){
			bool flag = add(i);
			if(!flag)	return false;
		}
	}
	return true;
}
point c1, c2;
inline bool cmp(const int &i, const int &j){
	point a = q[i] - p[i];
	point b = q[j] - p[j];
	if (sign(a.X) >= 0  && sign(b.X) < 0)
		return false;
	if (sign(a.X) < 0 && sign(b.X) >= 0)
		return true;
	if (sign(a.X) == 0 && sign(b.X) == 0){
		if (sign(a.Y) >= 0 || sign(b.Y) >= 0)
			return a.Y < b.Y;
		return b.Y < a.Y;
	}
	double det = a.X * b.Y - b.X * a.Y;
	if (sign(det) < 0)
		return false;
	if (sign(det) > 0)
		return true;
	int S = sign(cross(q[j] - p[j], q[i] - p[j]));
	if(S)	return S > 0;
	double d1 = a.X * a.X  + a.Y * a.Y;
	double d2 = b.X * b.X + b.Y * b.Y;
	return d1 < d2;
}
inline bool checker(point C1, point C2, double r){
	For(i,0,n){
		int j = (i + 1) % n;
		if(cross(p[j] - p[i], C1 - p[i]) < -1e-5)	return false;
		if(cross(p[j] - p[i], C2 - p[i]) < -1e-5)	return false;
		if(min(dis(p[i], p[j], C1), dis(p[i], p[j], C2)) > r + 1e-6)
			return false;
	}
	return true;
}
inline bool check(double x, bool cer = false){
	For(i,0,n){
		int j = (i + 1) % n;
		perm[i] = i;
		perm[i + n] = i + n;
		p[i] = point(pol[i].x, pol[i].y);
		q[i] = point(pol[j].x, pol[j].y);
		point s = shift(q[i] - p[i], x);
		q[i + n] = p[i] + s;
		p[i + n] = q[i] + s;
	}
	sort(perm, perm + 2 * n, cmp);
	fill(len, len + n, 0);
	int l = 0;
	For(i,0,n){
		while(l < n && canDo(i, (i+l)%n))	++ l;
		if(l < 1)	return false;
		if(l == n){
			if(cer){
				relax();
				For(j,st,en)
					if(checker(que[j], que[j], x)){
						c1 = c2 = que[j];
						return true;
					}
				return false;
			}
			return true;
		}
		len[i] = l;
		-- l;
	}
	For(i,0,n){
		int j = (i + len[i]) % n;
		if(len[i] + len[j] >= n){
			if(cer){
				canDo(i,(j-1+n) % n);
				relax();
				vector<point> V;
				For(e,st,en)	V.pb(que[e]);
				canDo(j, (i-1+n) % n);
				relax();
				do{
					c1 = V[rand() % (int)V.size()];
					c2 = que[st + rand() % (en-st)];
				}while(!checker(c1, c2, x));
			}
			return true;
		}
	}
	return false;
}
int main(){
	iOS;
	cerr << fixed << setprecision(10);
	cin >> n;
	For(i,0,n)	cin >> pol[i].x >> pol[i].y;
	double lo = 0, hi = 2e4 + 100;
	while(hi - lo > 1e-7){
		double mid = (lo + hi)/2.;
		if(check(mid))
			hi = mid;
		else
			lo = mid;
	}
	cout << fixed << setprecision(30);
	cout << lo << endl;
	error(check(lo + 2e-7, true));
	cout << c1.X << ' ' << c1.Y << '\n';
	cout << c2.X << ' ' << c2.Y << endl;
	return 0;
}

