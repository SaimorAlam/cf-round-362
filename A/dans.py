q = int(raw_input())
cost = {}

def getcost(u):
	if u in cost:
		return cost[u]
	else:
		return 0

for i in range(0, q):
	tt = raw_input().split(' ')
	u = int(tt[1])
	v = int(tt[2])
	if tt[0] == '2':
		res = 0
		while u != v:
			if u > v:
				res = res + getcost(u)
				u = u // 2
			else:
				res = res + getcost(v)
				v = v // 2
		print(res)
	else:
		w = int(tt[3])
		while u != v:
			if u > v:
				cost[u] = getcost(u) + w
				u = u // 2
			else:
				cost[v] = getcost(v) + w
				v = v // 2
