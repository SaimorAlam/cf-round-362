import java.io.OutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.StringTokenizer;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;

/**
 * Built using CHelper plug-in
 * Actual solution is at the top
 *
 * @author AlexFetisov
 */
public class Main {
    public static void main(String[] args) {
        InputStream inputStream = System.in;
        OutputStream outputStream = System.out;
        InputReader in = new InputReader(inputStream);
        PrintWriter out = new PrintWriter(outputStream);
        TaskB1_362 solver = new TaskB1_362();
        solver.solve(1, in, out);
        out.close();
    }

    static class TaskB1_362 {
        Graph g;
        double[] f;
        int[] am;

        public void solve(int testNumber, InputReader in, PrintWriter out) {
            int n = in.nextInt();
            int[] parent = new int[n];
            parent[0] = -1;
            g = new Graph();
            g.initGraph(n, n - 1);
            for (int i = 1; i < n; ++i) {
                parent[i] = in.nextInt() - 1;
                g.addDirectEdge(parent[i], i);
            }
            f = new double[n];
            am = new int[n];
            dfsAm(0);
            dfs(0, 1.0);
            for (int i = 0; i < n; ++i) {
                out.print(f[i] + " ");
            }
            out.println();
        }

        int dfsAm(int v) {
            am[v] = 1;
            for (int ei = g.first[v]; ei != -1; ei = g.next[ei]) {
                am[v] += dfsAm(g.to[ei]);
            }
            return am[v];
        }

        void dfs(int v, double ave) {
            f[v] = ave;
            int sum = 0;
            for (int ei = g.first[v]; ei != -1; ei = g.next[ei]) {
                sum += am[g.to[ei]];
            }
            for (int ei = g.first[v]; ei != -1; ei = g.next[ei]) {
                sum -= am[g.to[ei]];
                dfs(g.to[ei], ave + 1.0 + sum / 2.);
                sum += am[g.to[ei]];
            }
        }

    }

    static class InputReader {
        private BufferedReader reader;
        private StringTokenizer stt;

        public InputReader(InputStream stream) {
            reader = new BufferedReader(new InputStreamReader(stream));
        }

        public String nextLine() {
            try {
                return reader.readLine();
            } catch (IOException e) {
                return null;
            }
        }

        public String nextString() {
            while (stt == null || !stt.hasMoreTokens()) {
                stt = new StringTokenizer(nextLine());
            }
            return stt.nextToken();
        }

        public int nextInt() {
            return Integer.parseInt(nextString());
        }

    }

    static class Graph {
        public int[] from;
        public int[] to;
        public int[] first;
        public int[] next;
        public int nVertex;
        public int nEdges;
        public int curEdge;

        public Graph() {
        }

        public void initGraph(int n, int m) {
            curEdge = 0;
            nVertex = n;
            nEdges = m;
            from = new int[m * 2];
            to = new int[m * 2];
            first = new int[n];
            next = new int[m * 2];
            Arrays.fill(first, -1);
        }

        public void addDirectEdge(int a, int b) {
            next[curEdge] = first[a];
            first[a] = curEdge;
            to[curEdge] = b;
            from[curEdge] = a;
            ++curEdge;
        }

    }
}

