#include <bits/stdc++.h>
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
using namespace __gnu_pbds;
using namespace std;
#define Foreach(i, c) for(__typeof((c).begin()) i = (c).begin(); i != (c).end(); ++i)
#define For(i,a,b) for(int (i)=(a);(i) < (b); ++(i))
#define rof(i,a,b) for(int (i)=(a);(i) > (b); --(i))
#define rep(i, c) for(auto &(i) : (c))
#define x first
#define y second
#define pb push_back
#define PB pop_back()
#define iOS ios_base::sync_with_stdio(false)
#define sqr(a) (((a) * (a)))
#define all(a) a.begin() , a.end()
#define error(x) cerr << #x << " = " << (x) <<endl
#define Error(a,b) cerr<<"( "<<#a<<" , "<<#b<<" ) = ( "<<(a)<<" , "<<(b)<<" )\n";
#define errop(a) cerr<<#a<<" = ( "<<((a).x)<<" , "<<((a).y)<<" )\n";
#define coud(a,b) cout<<fixed << setprecision((b)) << (a)
#define L(x) ((x)<<1)
#define R(x) (((x)<<1)+1)
#define umap unordered_map
//#define double long double
typedef long long ll;
typedef pair<int,int>pii;
typedef vector<int> vi;
typedef complex<double> point;
template <typename T> using os =  tree<T, null_type, less<T>, rb_tree_tag, tree_order_statistics_node_update>;
template <class T>  inline void smax(T &x,T y){ x = max((x), (y));}
template <class T>  inline void smin(T &x,T y){ x = min((x), (y));}
const int maxn = 1e5 + 100;
int ans[maxn];
vi adj[maxn];
int sz[maxn];
inline void DFS(int v = 0){
	sz[v] = 1;
	rep(u, adj[v]){
		DFS(u);
		sz[v] += sz[u];
	}
}
inline void dfs(int v = 0, double e = 0){
	e += 2;
	ans[v] = e;
	int s = sz[v] - 1;
	rep(u, adj[v]){
		double x = s - sz[u];
		dfs(u, e + x);
	}
}
int main(){
	int n;
	scanf("%d", &n);
	For(i,1,n){
		int p;
		scanf("%d", &p);
		adj[--p].pb(i);
	}
	DFS();
	dfs();
	For(i,0,n)	printf("%.1f ",  (double)ans[i]/2.);
	puts("");
	return 0;
}